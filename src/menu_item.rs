use std::fmt::{self, Display, Formatter};
use crate::state::State;
use tui::layout::{Rect, Layout, Direction, Constraint};
use tui::Frame;
use tui::widgets::{Widget, StatefulWidget, Paragraph, Borders, Block, ListItem, List};
use tui::backend::Backend;
use tui::style::{Style, Color, Modifier};


#[derive(Clone)]
pub enum MenuItem {
    Config,
    GiftLists
}

impl Display for MenuItem {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match &self {
            MenuItem::Config => "Config",
            MenuItem::GiftLists => "Gift Lists",
        })
    }
}

impl From<&MenuItem> for usize {
    fn from(input: &MenuItem) -> usize {
        match input {
            MenuItem::Config => 0,
            MenuItem::GiftLists => 1,
        }
    }
}

pub trait Renderer {
    fn render<W>(&mut self, widget: W, area: Rect)
        where W: Widget;

    fn render_stateful<W>(&mut self, widget: W, area: Rect, state: &mut W::State)
        where W: StatefulWidget;
}

impl<T: Backend> Renderer for Frame<'_, T> {
    fn render<W>(&mut self, widget: W, area: Rect)
        where W: Widget {
        self.render_widget(widget, area);
    }

    fn render_stateful<W>(&mut self, widget: W, area: Rect, state: &mut W::State)
        where W: StatefulWidget {
        self.render_stateful_widget(widget, area, state);
    }
}

pub trait Drawable {
    fn draw<R>(&self, state: &mut State, target: Rect, renderer: &mut R)
        where R: Renderer;
}

impl Drawable for MenuItem {
    fn draw<R>(&self, state: &mut State, area: Rect, renderer: &mut R)
        where R: Renderer  {
        match &self {
            MenuItem::Config => {
                renderer.render(Paragraph::new("Example Config:\nDefault Gift List = pride"), area)
            },
            MenuItem::GiftLists => {
                if state.gift_lists_state().list().len() == 0 {
                    renderer.render(Paragraph::new("No gift lists available to display."), area);
                    return;
                }

                // Side bar //

                let max_gift_list_name_length = state.gift_lists_state().list().iter().map(|x| x.name().len()).max().unwrap() as u16;
                let chunks = Layout::default()
                    .margin(1)
                    .direction(Direction::Horizontal)
                    .constraints(
                        [
                            Constraint::Length(max_gift_list_name_length + 2), // 2 thingies for the border i believe..
                            Constraint::Min(10),
                        ].as_ref()
                    )
                    .split(area);

                let gift_lists: Vec<_> = state.gift_lists_state().list().iter()
                    .map(|list| ListItem::new(list.name().to_owned()))
                    .collect();

                let gift_list_menu = List::new(gift_lists)
                    .block(Block::default().title("Gift Lists").borders(Borders::ALL))
                    .highlight_style(Style::default()
                        .bg(Color::Yellow)
                        .fg(Color::Black)
                        .add_modifier(Modifier::BOLD));

                renderer.render_stateful(gift_list_menu, chunks[0], state.gift_lists_state_mut().list_state());

                // Gift list info //

                let selected_gift_list_name = state.gift_lists_state().current_gift_list().unwrap().name().to_owned();

                renderer.render(Paragraph::new("Coming Soon(ish)")
                                    .block(Block::default().title(selected_gift_list_name + " details").borders(Borders::ALL)), chunks[1]);
            },
        }
    }
}