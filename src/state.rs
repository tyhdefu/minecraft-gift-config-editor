use crate::menu_item::MenuItem;
use tui::widgets::ListState;

pub struct State {
    active_menu_item: MenuItem,
    gift_lists_state: GiftListsState
}

enum ScrollDirection {
    Up,
    Down
}

fn scroll(list_state: &mut ListState, length: usize, dir: ScrollDirection) {
    if length == 0 {
        list_state.select(None);
        return;
    }
    if let Some(mut cur) = list_state.selected() {
        let new = match dir {
            ScrollDirection::Up => {
                if cur == 0 { length - 1 } else { cur - 1 }
            }
            ScrollDirection::Down => {
                cur += 1;
                if cur == length { 0 } else { cur }
            }
        };
        list_state.select(Some(new));
    }
    else {
        list_state.select(Some(0));
        return;
    }
}

impl State {
    pub fn active_menu_item(&self) -> MenuItem {
        self.active_menu_item.clone()
    }

    pub fn set_active_menu_item(&mut self, menu_item: MenuItem) {
        self.active_menu_item = menu_item;
    }

    pub fn gift_lists_state(&self) -> &GiftListsState {
        &self.gift_lists_state
    }

    pub fn gift_lists_state_mut(&mut self) -> &mut GiftListsState {
        &mut self.gift_lists_state
    }

    pub fn up(&mut self) {
        self.scroll_current(ScrollDirection::Up)
    }

    pub fn down(&mut self) {
        self.scroll_current(ScrollDirection::Down)
    }

    fn scroll_current(&mut self, dir: ScrollDirection) {
        match self.active_menu_item {
            MenuItem::GiftLists => {
                let len = self.gift_lists_state.lists.len();
                scroll(self.gift_lists_state.list_state(), len, dir);
            }
            _ => {}
        }
    }
}

impl Default for State {
    fn default() -> Self {
        State {
            active_menu_item: MenuItem::Config,
            gift_lists_state: GiftListsState::default(),
        }
    }
}

pub struct GiftList {
    name: String
    // TODO:
}

impl GiftList {
    pub fn new(name: String) -> Self {
        GiftList {
            name
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}

pub struct GiftListsState {
    lists: Vec<GiftList>,
    selected_list: ListState,
}

impl Default for GiftListsState {
    fn default() -> Self {
        GiftListsState {
            lists: vec![GiftList::new("example_1".to_owned()), GiftList::new("example_2".to_owned()), GiftList::new("long_example".to_owned())],
            selected_list: ListState::default()
        }
    }
}

impl GiftListsState {
    pub fn list(&self) -> &Vec<GiftList> {
        &self.lists
    }

    pub fn list_state(&mut self) -> &mut ListState {
        &mut self.selected_list
    }

    pub fn current_gift_list(&self) -> Option<&GiftList> {
        if self.lists.len() == 0 {
            return None;
        }
        self.lists.get(self.selected_list.selected().unwrap_or(0))
    }
}