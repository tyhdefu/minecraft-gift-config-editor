mod state;
mod menu_item;

use crossterm::terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen};
use std::{io, thread};
use tui::backend::CrosstermBackend;
use tui::Terminal;
use std::sync::mpsc::{Sender, TryRecvError};
use std::time::{Duration, Instant};
use crossterm::event;
use crossterm::event::{Event, KeyEvent, KeyCode};
use std::sync::mpsc;
use crossterm::execute;
use tui::layout::{Layout, Direction, Constraint};
use std::thread::sleep;
use tui::widgets::{Tabs, Block, Borders};
use crossterm::event::KeyCode::Char;
use crate::state::State;
use tui::style::{Style, Color, Modifier};
use crate::menu_item::{MenuItem, Drawable};
use tui::text::{Spans, Span};

enum InputMessageEvent {
    Input(KeyEvent),
}


fn spawn_input_thread(tx: Sender<InputMessageEvent>) {
    let tick_rate = Duration::from_millis(200);
    thread::spawn(move || {
        let mut last_tick = Instant::now();
        loop {
            let timeout = tick_rate
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout).expect("poll works") {
                if let Event::Key(key) = event::read().expect("can read events") {
                    tx.send(InputMessageEvent::Input(key)).expect("can send events");
                }
            }

            //if last_tick.elapsed() >= tick_rate {
            //    if let Ok(_) = tx.send(InputMessageEvent::Tick) {
            //        last_tick = Instant::now();
            //    }
            //}
        }
    });
}

fn main() -> Result<(), io::Error> {
    println!("Hello, world!");
    enable_raw_mode()?;

    let (tx, rx) = mpsc::channel();
    spawn_input_thread(tx);

    let stdout = io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;
    execute!(io::stdout(), EnterAlternateScreen).unwrap();

    let refresh_delay = Duration::from_millis(1000 / 30); // 30 fps mm

    let mut state = State::default();
    let menu_titles = vec![MenuItem::Config.to_string(), MenuItem::GiftLists.to_string()];

    loop {

        // Rendering //

        terminal.draw(|frame| {
            let rect = frame.size();
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(2)
                .constraints(
                    [
                        Constraint::Length(3),
                        Constraint::Min(3),
                    ].as_ref()
                )
                .split(rect);

            let menu_options = menu_titles.iter()
                .map(|t| {
                    let (first, rest) = t.split_at(1);
                    Spans::from(vec![
                        Span::styled(
                            first,
                            Style::default()
                                .fg(Color::Cyan)
                                .add_modifier(Modifier::UNDERLINED)
                        ),
                        Span::styled(rest, Style::default()),
                    ])
                })
                .collect();

            let tabs = Tabs::new(menu_options)
                .select((&state.active_menu_item()).into())
                .block(Block::default().title("Menu").borders(Borders::ALL))
                .style(Style::default().fg(Color::White))
                .highlight_style(Style::default().fg(Color::Green))
                .divider(Span::raw("|"));

            frame.render_widget(tabs, chunks[0]);
            state.active_menu_item().draw(&mut state, chunks[1], frame);
        })?;

        // Handle Input //

        match rx.try_recv() {
            Ok(message) => {
                match message {
                    InputMessageEvent::Input(key) => {
                        match key.code {
                            Char('q') => {
                                println!("q pressed, exiting");
                                break;
                            },
                            Char('c') => state.set_active_menu_item(MenuItem::Config),
                            Char('g') => state.set_active_menu_item(MenuItem::GiftLists),
                            KeyCode::Up => state.up(),
                            KeyCode::Down => state.down(),
                            _ => {}
                        }
                    }
                }
            },
            Err(TryRecvError::Empty) => {}
            Err(TryRecvError::Disconnected) => {
                println!("Input thread disconnected.");
                break;
            }
        }

        // Delay //

        sleep(refresh_delay); // technically we should wait not as long if it took a while to render but shh.
    }

    execute!(io::stdout(), LeaveAlternateScreen).unwrap();
    disable_raw_mode()?;
    return Ok(());
}
